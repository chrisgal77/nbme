# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# +
from dataclasses import dataclass, field
from pathlib import Path
import re
from typing import Iterator, Union, Optional

import pandas as pd
import plotly.express as px
import pytorch_lightning as pl
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from transformers import (RobertaTokenizerFast,
                          RobertaForTokenClassification)
from datasets import load_metric
from torch.utils.data import random_split
from torch.optim import AdamW
from torch.optim.lr_scheduler import ReduceLROnPlateau
from pytorch_lightning.callbacks import LearningRateMonitor
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning import Trainer
import spacy
from accelerate import Accelerator
from spacy import displacy
from datasets import load_metric

# -

# ## Load data

CORE_PATH = Path(
    "/Users/gal/Documents/projects/pdzd/nbme/nbme-score-clinical-patient-notes"
)
TRAIN_PATH = CORE_PATH / "train.csv"
FEATURES_PATH = CORE_PATH / "features.csv"
NOTES_PATH = CORE_PATH / "patient_notes.csv"

# +
train_df = pd.read_csv(TRAIN_PATH)
notes_df = pd.read_csv(NOTES_PATH)
features_df = pd.read_csv(FEATURES_PATH)

train_df.shape, notes_df.shape, features_df.shape
# -

# ## Define training example

# +
Location = list[int]


def map_location_to_list(location_str: str) -> Location:
    number_pattern = re.compile(r"""\d+""")
    all_numbers = re.findall(number_pattern, location_str)
    locations_list = [
        (int(all_numbers[i]), int(all_numbers[i + 1]))
        for i in range(len(all_numbers[::2]))
    ]
    return locations_list


def map_annotations_to_list(annotations_str: str) -> list[str]:
    clipped = annotations_str[2:-2]
    cleaned = re.sub(r"'", "", clipped)
    return re.split(r",", cleaned)


# -

train_df[train_df["pn_num"] == 16]["location"][0]


@dataclass(frozen=True)
class Annotation:
    text: str = field(repr=True)
    location: Location = field(repr=True)
    potencial_wrong: bool = field(repr=True)

    @classmethod
    def from_note_annotations(
        cls, input_annotations: list[str], locations: Location
    ) -> list["Annotation"]:
        annotations = []

        for annotation, location in zip(input_annotations, locations):
            is_wrong = location[1] - location[0] != len(annotation)
            annotations.append(cls(text=annotation, location=location, potencial_wrong=is_wrong))

        return annotations


@dataclass(frozen=True)
class PatientNote:

    case_num: int
    note: str
    pn_num: int
    annotations: list[Annotation]
    features: list[int]


class ExampleCreator:
    def __init__(
        self,
        train_dataframe: pd.DataFrame,
        note_dataframe: pd.DataFrame,
        features_dataframe: pd.DataFrame,
    ):
        self.train_dataframe = train_dataframe
        self.note_dataframe = note_dataframe
        self.features_dataframe = features_dataframe

    def generate_examples(self) -> Iterator[PatientNote]:
        note_numbers = set(self.note_dataframe["pn_num"])
        for note_number in note_numbers:
            note_info = self.note_dataframe[
                self.note_dataframe["pn_num"] == note_number
            ]
            case_num = note_info["case_num"].item()
            note = note_info["pn_history"].item()
            annotations = list(
                self.train_dataframe[self.train_dataframe["pn_num"] == note_number][
                    "annotation"
                ].map(map_annotations_to_list)
            )
            locations = list(
                self.train_dataframe[self.train_dataframe["pn_num"] == note_number][
                    "location"
                ].map(map_location_to_list)
            )
            features_list = list(
                self.train_dataframe[self.train_dataframe["pn_num"] == note_number][
                    "feature_num"
                ]
            )

            annotations = [
                    Annotation.from_note_annotations(input_annotations=text, locations=location)
                    for text, location in zip(annotations, locations)
                    # if text
            ]

            features = [self.features_dataframe[self.features_dataframe["feature_num"] == idx]["feature_text"].item() for idx in features_list]

            example = PatientNote(
                case_num=case_num,
                note=note,
                pn_num=note_number,
                annotations=annotations,
                features=features,
            )

            yield example


example_generator = ExampleCreator(train_df, notes_df, features_df)
examples = [example for example in example_generator.generate_examples()]

# # EDA

for annotation in examples[16].annotations:
    print(annotation)

examples[16].annotations.__len__(), examples[16].features.__len__()

# +
annotated_idx = []
annotated = 0
for idx, example in enumerate(examples):
    if example.annotations:
        annotated += 1
        annotated_idx.append(idx)

px.pie(
    values=[annotated, len(examples) - annotated],
    names=["annotated", "not annotated"],
    title="Percentage of annotated notes",
)


# + colab={"base_uri": "https://localhost:8080/", "height": 542} id="xbOXOaTorDnj" executionInfo={"status": "ok", "timestamp": 1653128962642, "user_tz": -120, "elapsed": 1347, "user": {"displayName": "Bartek", "userId": "08284664871254345910"}} outputId="c01a81f3-b888-4c1c-d1bb-1eeee93e0809"
list_of_usless=[]
beggings_idx=[]
number_of_words_in_annotation=[]
length_dep_on_case={}
for example in examples:
    if example.case_num not in length_dep_on_case:
        length_dep_on_case[example.case_num]=[]
    length_dep_on_case[example.case_num].append(len(str.split((example.note))))
    if(len(example.annotations)):
        lenght=len(example.note)
        informations_len=0
        for ann in example.annotations :
            for case in ann :
                informations_len+=len(case.text)
                number_of_words_in_annotation.append(len(str.split(case.text)))
                beggings_idx.append(case.location[0])
        list_of_usless.append((lenght-informations_len)/lenght)
px.histogram(list_of_usless,title="procentage of words not in annotations")

# + colab={"base_uri": "https://localhost:8080/", "height": 542} id="pbgkA0rKymF2" executionInfo={"status": "ok", "timestamp": 1653129570638, "user_tz": -120, "elapsed": 438, "user": {"displayName": "Bartek", "userId": "08284664871254345910"}} outputId="2ae023c6-a353-4503-b68c-45b2eeee3af4"
px.histogram(beggings_idx,title="placement of annotation",nbins=100)

# + colab={"base_uri": "https://localhost:8080/", "height": 542} id="PznQOso3ymNX" executionInfo={"status": "ok", "timestamp": 1652723414091, "user_tz": -120, "elapsed": 1081, "user": {"displayName": "Bartek", "userId": "08284664871254345910"}} outputId="62e7f89a-794d-4bee-ab6e-ee36c9c0288f"
import plotly.graph_objects as go
fig=go.Figure()
for el in length_dep_on_case:
  fig.add_trace(go.Histogram(x=length_dep_on_case[el]))
fig.update_layout(barmode='overlay')
fig.update_traces(opacity=0.75)
fig.show()

# + colab={"base_uri": "https://localhost:8080/", "height": 542} id="Ae9-rsoUymUh" executionInfo={"status": "ok", "timestamp": 1652723051443, "user_tz": -120, "elapsed": 269, "user": {"displayName": "Bartek", "userId": "08284664871254345910"}} outputId="924abe06-929e-413c-f6b8-05ae775b5407"
px.histogram(number_of_words_in_annotation,nbins=32,title="ammount of words in annotation")

# + [markdown] id="FYKHuh0S8fx4"
# wordcloud

# + id="nVV6BCz_8fGK"
# import wordcloud
# import matplotlib.pyplot as plt
# case_notes = {}
# for example in examples:
#     if example.case_num not in case_notes:
#         case_notes[example.case_num]=[]
#     case_notes[example.case_num].append(example.note)
# for case in case_notes :
#   wordcloud_notes = wordcloud.WordCloud(stopwords=wordcloud.STOPWORDS, max_font_size=120, max_words=5000,
#                         width = 300, height = 200,
#                         background_color='white').generate(" ".join(case_notes[case]))
#   fig, ax = plt.subplots(figsize=(14,10))
#   ax.imshow(wordcloud_notes, interpolation='bilinear')
#   ax.set_axis_off()
#   plt.imshow(wordcloud_notes);

# + [markdown] id="63F3nYWhBEqO"
# wrong annotations

# + id="kkSYlF1UBHQL"
counter={"annotations":0,"wrong_ann":0}
for element in examples:
  if (len(element.annotations)):
    for ann in element.annotations:
      if(len(ann)):
        counter["annotations"]+=1
        if(ann[0].potencial_wrong):
         counter["wrong_ann"]+=1
px.pie(
    values=[counter["wrong_ann"], counter["annotations"]-counter["wrong_ann"]],
    names=["correct annotations", "wrong annotations"],
    title="Percentage of correct annotations",
)


# + [markdown] id="T_f08hCWHPF5"
# Position of most frequent words in case 

# + colab={"base_uri": "https://localhost:8080/", "height": 542} id="U3tJMc7UIYSx" executionInfo={"status": "ok", "timestamp": 1653132801031, "user_tz": -120, "elapsed": 652, "user": {"displayName": "Bartek", "userId": "08284664871254345910"}} outputId="966544b4-5490-48ae-8177-6e55e8150fcd"
import numpy as np
from collections import Counter
how_many_in_case=[]
word_counters=[]
for case in np.unique(notes_df["case_num"]):
 cases=[example for example in examples if example.case_num==case]
 how_many_in_case.append(len(cases))
#  pointless cuz creating short words
#  c=Counter([word for example in cases for word in example.note.split()])
#  df_temp = pd.DataFrame(c.most_common(10))
#  df_temp.columns = ['Common_words','count']
#  fig = px.bar(df_temp, x='count', y='Common_words', title='Most Common Words(excluding stopwords) in Annotations', orientation='h', width=900,height=700, color='Common_words')
#  fig.show()
px.bar(how_many_in_case)
  

# + colab={"base_uri": "https://localhost:8080/", "output_embedded_package_id": "1xQLeH21xuAJ4WdbiftpyQ4YG1Hc-wXFe"} id="ZRUh6xbFJUKc" executionInfo={"status": "ok", "timestamp": 1653128792828, "user_tz": -120, "elapsed": 4722, "user": {"displayName": "Bartek", "userId": "08284664871254345910"}} outputId="b70ab3be-ffb3-44a0-a74f-7e38efee4a45"
type([print(el.note) for el in examples])

# + colab={"base_uri": "https://localhost:8080/", "height": 402} id="rvtawEgYoXhs" executionInfo={"status": "ok", "timestamp": 1652718019257, "user_tz": -120, "elapsed": 234, "user": {"displayName": "Bartek", "userId": "08284664871254345910"}} outputId="7f106984-78fa-4719-c78a-3287608637e7"
example_num = 79

ents = []
for annotation, feature in zip(examples[example_num].annotations, examples[example_num].features):

    for single_annotation in annotation:
            ents.append({
                'start': single_annotation.location[0], 
                'end' : single_annotation.location[1],
                "label" : feature
            })
    
doc = {
    'text' : examples[example_num].note,
    "ents" : ents
}
colors = {"Annotation" :"linear-gradient(90deg, #aa9cfc, #fc9ce7)" }
options = {"colors": colors}
spacy.displacy.render(doc, style="ent", options = options , manual=True, jupyter=True)
# -

# # Problem solution

# ### Unsupervised annotation finding


# +
class FeatureMapper:
    def __init__(self, labels: pd.DataFrame, unk_token: str = "O"):
        self.origin_labels = labels
        self.unk_token = unk_token
        labels_text = list(set(self.origin_labels["feature_text"]))
        prepared_texts = self._prepare_labels(labels_text)
        self.encoding_text = self._prepare_encode(prepared_texts + [unk_token])
        self.decoding_text = self._prepare_decode(prepared_texts + [unk_token])

    def encode_feature_text(self, label: int) -> int:
        return self.encoding_text[label]
    
    def decode_feature_text(self, label: int) -> int:
        return self.decoding_text[label]

    def _prepare_encode(self, labels: list[int]) -> dict[Union[str, int], int]:
        return dict(zip(labels, range(len(labels))))

    def _prepare_decode(self, labels: list[int]) -> dict[Union[str, int], int]:
        return dict(zip(range(len(labels)), labels))

    def _prepare_labels(self, labels: list[str]) -> list[str]:
        beginnings = ["B-"+label for label in labels]
        inside = ["I-"+label for label in labels]
        
        mixed = []
        for i in range(len(beginnings)):
            mixed.append(beginnings[i])
            mixed.append(inside[i])

        return mixed


mapping = FeatureMapper(features_df)
mapping.encoding_text
# mapping.encode_feature_text("Shortness-of-breath")
# -

list(mapping.encoding_text.values()).__len__()

# +
examples_for_dataset = [example for example in examples if example.annotations]

def crop_examples(example: PatientNote):
    
    left_limit = 10000
    right_limit = 0
    annotations = example.annotations
    for annotation in annotations:
        for inner in annotation:
            if inner.location[0] < left_limit:
                left_limit = inner.location[0]

            if inner.location[1] > right_limit:
                right_limit = inner.location[1]

    print(left_limit, right_limit)

    new_annotations = []
    for annotation in annotations:
        inner_annotations = []
        for inner in annotation:
            left, right = inner.location

            inner_annotations.append(
                Annotation(
                    text=inner.text,
                    location=[
                        left - left_limit,
                        right - left_limit
                    ],
                    potencial_wrong=inner.potencial_wrong
                )
            )
        new_annotations.append(inner_annotations)

    return PatientNote(
        case_num=example.case_num,
        note=example.note[left_limit:right_limit],
        pn_num=example.pn_num,
        annotations=new_annotations,
        features=example.features
    )

examples_for_dataset = [crop_examples(example) for example in examples_for_dataset]

# +
example_num = 0

ents = []


for annotation, feature in zip(examples_for_dataset[example_num].annotations, examples_for_dataset[example_num].features):
    for single_annotation in annotation:
            ents.append({
                'start': single_annotation.location[0], 
                'end' : single_annotation.location[1],
                "label" : feature
            })
    
doc = {
    'text' : examples_for_dataset[example_num].note,
    "ents" : ents
}
colors = {"Annotation" :"linear-gradient(90deg, #aa9cfc, #fc9ce7)" }
options = {"colors": colors}
spacy.displacy.render(doc, style="ent", options = options , manual=True, jupyter=True)

# +
PreparedExamples = dict[str, Union[list[str], list[int]]]


class BaseWordClassificationDataset:
    def __init__(self, examples: list[PatientNote], label_mapper: FeatureMapper) -> None:
        self.examples = examples
        self.mapping = label_mapper
        self.prepared_examples = self._prepare_annotations(self.examples)
        

    def __getitem__(self, index: int):
        data = self.prepared_examples.iloc[index]
        return {"word_tokens": data["tokens"], "labels": data["labels"]}

    def __len__(self) -> int:
        return len(self.examples)

    def _prepare_annotations(self, examples: list[PatientNote]) -> pd.DataFrame:
        prepared_examples: PreparedExamples = {"tokens": [], "labels": []}

        for example in examples:
            note_text = example.note
            annotations = example.annotations
            features = example.features

            word_tokens = re.finditer(r"\S+", note_text)
            labels = []
            for token in word_tokens:
                token_start, token_end = token.span()

                label = "O"
                for annotation, feature in zip(annotations, features):
                    for doubled_annotation in annotation:
                        annotation_start, annotation_end = doubled_annotation.location

                        if annotation_start > token_start:
                            break
                        
                        if token_start >= annotation_start and token_end <= annotation_end:
                            if label == "O":
                                label = feature
                label = "B-" + label if label != "O" else "O" 
                labels.append(label)

            prepared_examples["labels"].append(torch.LongTensor([self.mapping.encode_feature_text(label) for label in labels]))
            prepared_examples["tokens"].append([token.group(0) for token in re.finditer(r"\S+", note_text)])
        
        return pd.DataFrame(prepared_examples)


base_dataset = BaseWordClassificationDataset(examples_for_dataset, mapping)

# -

base_dataset[0]["word_tokens"][:10], base_dataset[0]["labels"][:10]


# +
class TokenizerWrapper:
    def __init__(self) -> None:
        self.tokenizer = RobertaTokenizerFast.from_pretrained("roberta-base", add_prefix_space=True)

    def __call__(self, input_words: list[str]):
        return self.tokenizer(input_words, return_tensors="pt", max_length=514, truncation=True, padding="max_length", is_split_into_words=True)

tokenizer = TokenizerWrapper()
tokenizer(base_dataset[0]["word_tokens"][:10]).word_ids()


# +
class WordClassificationDataset(Dataset):
    def __init__(self, tokenizer: TokenizerWrapper, examples: list[PatientNote], mapping: FeatureMapper) -> None:
        super().__init__()
        self.tokenizer = tokenizer
        self.dataset = BaseWordClassificationDataset(examples, mapping)

    def __getitem__(self, index: int) -> dict[str, Union[torch.Tensor, list[int]]]:
        return self._prepare_example(self.dataset[index])

    def __len__(self) -> int:
        return len(self.dataset)

    def _prepare_example(self, example: list[str, list[int]]) -> dict[str, list[int]]:
        word_tokens = example["word_tokens"]
        labels = example["labels"]

        tokenized = self.tokenizer(word_tokens)
        word_ids = tokenized.word_ids()

        prepare_labels = self._prepare_labels(labels, word_ids)

        for key, value in tokenized.items():
            tokenized[key] = torch.squeeze(value)

        tokenized.update({"labels": prepare_labels})

        return tokenized

    def _prepare_labels(self, labels: list[int], word_ids: list[Union[int, None]]) -> list[int]:
        new_labels = []
        current_word = None
        for word_id in word_ids:
            if word_id != current_word:
                current_word = word_id
                label = -100 if word_id is None else labels[word_id].item()
                new_labels.append(label)
            elif word_id is None:
                new_labels.append(-100)
            else:
                label = labels[word_id].item()
                if label % 2 == 0 and label != 262:
                    label += 1
                new_labels.append(label)

        return torch.LongTensor(new_labels)
                

dataset = WordClassificationDataset(tokenizer, examples_for_dataset, mapping)
dataset[0]["labels"]
# -

train_dataloader = DataLoader(dataset, batch_size=16, shuffle=True)

next(iter(train_dataloader))

# +
Embedding = torch.Tensor


class FeatureClassificationModel(nn.Module):
    def __init__(self, id2label: dict[int, str], label2id: dict[str, int]) -> None:
        super().__init__()
        self.classifier = RobertaForTokenClassification.from_pretrained(
            "roberta-base",
            id2label=id2label,
            label2id=label2id,
        )

    def __call__(self, input_tensor: dict[str, Union[torch.Tensor, list[int]]]) -> Embedding:
        return self.classifier(**input_tensor)


# -


class WordClassificationDatasetModule(pl.LightningDataModule):
    def __init__(self, batch_size: int, tokenizer: TokenizerWrapper, examples: list[PatientNote], mapping: FeatureMapper):
        super().__init__()
        self.batch_size = batch_size
        self.tokenizer = tokenizer
        self.examples = examples
        self.mapping = mapping

    def setup(self, stage: Optional[str] = None):
        dataset = WordClassificationDataset(self.tokenizer, self.examples, self.mapping)
        train_size = int(0.8 * len(dataset))
        eval_size = len(dataset) - train_size  
        self.train_dataset, eval_dataset = random_split(dataset, [train_size, eval_size])

        eval_size = int(0.5 * len(eval_dataset))
        test_size = len(dataset) - eval_size
        self.validation_dataset, self.test_dataset = random_split(dataset, [eval_size, test_size])

    def train_dataloader(self):
        return DataLoader(self.train_dataset, batch_size=self.batch_size, shuffle=True)

    def val_dataloader(self):
        return DataLoader(
            self.validation_dataset, batch_size=self.batch_size, shuffle=False
        )

    def test_dataloader(self):
        return DataLoader(self.test_dataset, batch_size=self.batch_size, shuffle=False)


def postprocess(predictions: torch.Tensor, labels: torch.Tensor, mapper: FeatureMapper):
    predictions = predictions.detach().cpu().clone().numpy()
    labels = labels.detach().cpu().clone().numpy()

    # Remove ignored index (special tokens) and convert to labels
    true_labels = [[mapper.decode_feature_text(l) for l in label if l != -100] for label in labels]
    true_predictions = [
        [mapper.decode_feature_text(p) for (p, l) in zip(prediction, label) if l != -100]
        for prediction, label in zip(predictions, labels)
    ]
    return true_labels, true_predictions


class PortraitSegmentationModel(pl.LightningModule):
    def __init__(self):
        super().__init__()
        self.mapper = FeatureMapper(features_df)
        self.metric = load_metric("seqeval")
        self.model = FeatureClassificationModel(self.mapper.decoding_text, self.mapper.encoding_text)
        self.accelerator = Accelerator()

    def forward(self, input_batch: dict[str, torch.Tensor]) -> torch.Tensor:
        return self.model(input_batch)

    def shared_step(self, batch: dict[str, torch.Tensor]) -> float:
        labels = batch["labels"]
        scores = self(batch)

        preds = self.accelerator.pad_across_processes(scores.logits.argmax(dim=-1), dim=1, pad_index=-100)
        labels = self.accelerator.pad_across_processes(labels, dim=1, pad_index=-100)

        predictions_gathered = self.accelerator.gather(preds)
        labels_gathered = self.accelerator.gather(labels)

        true_predictions, true_labels = postprocess(predictions_gathered, labels_gathered, self.mapper)
        self.metric.add_batch(predictions=true_predictions, references=true_labels)

        results = self.metric.compute()
        return {"loss": scores.loss} | results

    def training_step(self, batch: torch.Tensor, batch_idx: int) -> dict[str, float]:
        results = self.shared_step(batch)
        self.log('train_loss', results["loss"])
        return results

    def validation_step(self, batch: torch.Tensor, batch_idx: int) -> dict[str, float]:
        results = self.shared_step(batch)
        self.log('validation_loss', results["loss"])
        return {"val_"+key: value for key, value in results.items()}#, "validation_acc": results["accuracy"]}

    def configure_optimizers(self):
        optimizer = AdamW(self.parameters(), lr=1e-3)
        scheduler = ReduceLROnPlateau(optimizer, mode="min", factor=0.2, patience=20, min_lr=5e-5)
        return {"optimizer": optimizer, "lr_scheduler": scheduler, "monitor": "validation_loss"}



# +
roberta_model = FeatureClassificationModel(mapping.decoding_text, mapping.encoding_text)
model = PortraitSegmentationModel()

data_module = WordClassificationDatasetModule(8, tokenizer, examples_for_dataset, mapping)

trainer = Trainer(
    progress_bar_refresh_rate=1,
    max_epochs=10,
    gpus=0,
    logger=TensorBoardLogger("lightning_logs/", name="roberta"),
    callbacks=[LearningRateMonitor(logging_interval="step")],
)

trainer.fit(model, datamodule=data_module)
# -

model_path = "/Users/gal/Documents/projects/pdzd/nbme/epoch=20-step=2100.ckpt"
model = PortraitSegmentationModel.load_from_checkpoint(model_path)

examples_for_dataset[0].note

# +
x = model(tokenizer(examples_for_dataset[0].note.split(" "))).logits.argmax(2)

x.squeeze().shape
# -

dataset[0]["labels"]

[mapping.decode_feature_text(x.item()) for x in x.squeeze()]

examples[0]

df1 = pd.read_parquet("/Users/gal/Documents/projects/pdzd/nbme/evaluations/0-1.parquet")
df1.iloc[1]
notes_df[notes_df["pn_num"] == 1]["pn_history"].item()

df1.iloc[1].labels

df1.iloc[0].labels


